<?php

use Illuminate\Database\Seeder;

class MasterCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_cities')->insert([
            'name' => 'Chandigarh'
        ]);
        DB::table('master_cities')->insert([
            'name' => 'Delhi'
        ]);
        DB::table('master_cities')->insert([
            'name' => 'Bangalore'
        ]);
        DB::table('master_cities')->insert([
            'name' => 'Pune'
        ]);
        DB::table('master_cities')->insert([
            'name' => 'Hyderabad'
        ]);
    }
}
