<?php

namespace App\Manager;

use App\Entity\User;

class UserManager
{
    public function createNewUser($data)
    {
        try {
            $user = new User($data);
            $user->save();

            return array(
                'message' => 'Successfully Added User',
                'status_code' => 200
            );
        } catch (\Exception $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                $message = 'Duplicate Entry';
                $statusCode = 400;
            } else {
                $message = $e->getMessage();
                $statusCode = 500;
            }
            return array(
                'message' => $message,
                'status_code' => $statusCode
            );
        }
    }
}
