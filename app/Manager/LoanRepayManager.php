<?php

namespace App\Manager;

use App\Entity\UserLoans;
use App\Entity\Repayments;
use Illuminate\Support\Facades\DB;

class LoanRepayManager
{
    public function repayLoanInstallment($loanId)
    {
        $loanDetails = UserLoans::find($loanId);
        if (empty($loanDetails)) {
            return array(
                'message' => 'Loan Not Found',
                'status_code' => 404
            );
        }
        if ($loanDetails->loan_repaid) {
            return array(
                'message' => 'Loan Is Already Repaid For',
                'status_code' => 400
            );
        }

        $rowToUpdate = Repayments::where('loan_id', $loanId)->where('paid_date', null)->first();
        if (!empty($rowToUpdate)) {
            $rowToUpdate->update(array('paid_date' => DB::raw('NOW()')));
        }

        $paymentsLeft = Repayments::where('loan_id', $loanId)->where('paid_date', null)->count();
        if ($paymentsLeft === 0) {
            $loanDetails->update(array('loan_repaid' => 1));
        }

        return array(
            'message' => 'Loan Installment Successfully Paid For',
            'status_code' => 200,
            'transaction_id' => $rowToUpdate->id
        );
    }
}
