<?php

namespace App\Manager;

use Illuminate\Http\Request;
use App\Entity\UserLoans;
use App\Entity\Repayments;

class LoanManager
{
    public function createNewLoan($data)
    {
        $loanAmount = $data['principal_amount'] + $data['processing_fee'];
        $term = $data['repayment_count'];
        switch ($data['payment_frequency']) {
            case 'MONTHLY':
                $apr = $data['interest_rate'] / 1200;
                $dueDates = array();
                $date = date('Y-m-d', strtotime('+1 month', strtotime(date("Y-m-d"))));
                for ($i=1; $i <= $term; $i++, $date = date('Y-m-d', strtotime('+1 month', strtotime($date)))) {
                    array_push($dueDates, $date);
                }
                break;
            case 'YEARLY':
                $apr = $data['interest_rate'] / 100;
                $dueDates = array();
                $date = date('Y-m-d', strtotime('+1 year', strtotime(date("Y-m-d"))));
                for ($i=1; $i <= $term; $i++, $date = date('Y-m-d', strtotime('+1 year', strtotime($date)))) {
                    array_push($dueDates, $date);
                }
                break;
        }
        $emi = $apr * -$loanAmount * pow((1 + $apr), $term) / (1 - pow((1 + $apr), $term));
        $installmentAmount = round($emi);
        $totalAmount = $installmentAmount * $term;
        $data['repayment_amount'] = $totalAmount;
        try {
            $loan = new UserLoans($data);
            $loan->save();

            $loanId = $loan->id;

            $repayments = $this->getRepaymentsData($installmentAmount, $dueDates, $loanId);

            foreach ($repayments as $repayment) {
                $repaymentObject = new Repayments($repayment);
                $repaymentObject->save();
            }

            return array(
                'message' => 'Successfully Added Loan',
                'repayments_info' => $repayments,
                'loan_id' => $loanId,
                'status_code' => 200
            );
        } catch (\Exception $e) {
            return array(
                'message' => $e->getMessage(),
                'status_code' => 500
            );
        }
    }

    public function getRepaymentsData($installmentAmount, $dueDates, $loanId)
    {
        $repayments = array();
        foreach ($dueDates as $dueDate) {
            $repaymentData = array(
                'due_date' => $dueDate,
                'loan_id' => $loanId,
                'amount_pending' => $installmentAmount
            );
            array_push($repayments, $repaymentData);
        }

        return $repayments;
    }
}
