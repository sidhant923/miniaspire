<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Repayments extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'due_date', 'loan_id', 'amount_pending', 'paid_date'
    ];

    public function userLoan()
	{
    	return $this->belongsTo(UserLoans::class);
	}
}
