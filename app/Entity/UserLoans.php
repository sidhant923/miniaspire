<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class UserLoans extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'principal_amount', 'interest_rate', 'user_id', 'payment_frequency', 'repayment_count', 'processing_fee', 'loan_repaid', 'repayment_amount'
    ];

    public function user()
	{
    	return $this->belongsTo(User::class);
	}

	public function repayments()
	{
    	return $this->hasMany(Repayments::class);
	}
}
