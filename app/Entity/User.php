<?php

namespace App\Entity;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'city_id'
    ];

    public function city()
	{
    	return $this->belongsTo(MasterCities::class);
	}

	public function loans()
	{
    	return $this->hasMany(UserLoans::class);
	}
}
