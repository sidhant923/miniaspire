<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manager\LoanManager;

class LoansController extends Controller
{
    protected $loanManager;

	public function __construct(LoanManager $loanManager)
    {
        $this->loanManager = $loanManager;
    }

    public function createNewLoan(Request $request)
    {

    	try {
    		$data = $request->validate([
        		'principal_amount' => 'required|integer',
        		'interest_rate' => 'required|between:0.00,100.00',
        		'user_id' => 'required|integer',
        		'payment_frequency' => 'required|in:YEARLY,MONTHLY',
        		'repayment_count' => 'required|integer',
        		'processing_fee' => 'required|integer'
    		]);
    	} catch (\Exception $e) {
    		return response()->json([
    			'message' => $e->getMessage(),
    			'status_code' => 400
			], 400);
    	}

    	$response = $this->loanManager->createNewLoan($data);

    	return response()->json(
    		$response
    	, $response['status_code']);
    }
}
