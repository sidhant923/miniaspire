<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manager\UserManager;

class UsersController extends Controller
{

	protected $userManager;

	public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function createNewUser(Request $request)
    {

    	try {
    		$data = $request->validate([
        		'first_name' => 'required',
        		'last_name' => 'required',
        		'email' => 'required|email',
        		'phone' => 'required',
        		'city_id' => 'required',
    		]);
    	} catch (\Exception $e) {
    		return response()->json([
    			'message' => $e->getMessage(),
    			'status_code' => 400
			], 400);
    	}

    	$response = $this->userManager->createNewUser($data);

    	return response()->json(
    		$response
    	, $response['status_code']);
    }
}
