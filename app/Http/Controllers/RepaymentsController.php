<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manager\LoanRepayManager;

class RepaymentsController extends Controller
{
    protected $loanRepayManager;

	public function __construct(LoanRepayManager $loanRepayManager)
    {
        $this->loanRepayManager = $loanRepayManager;
    }

    public function loanRepayment(Request $request)
    {
        $loanId = $request->validate([
            'loan_id' => 'required|integer'
        ])['loan_id'];

        $response = $this->loanRepayManager->repayLoanInstallment($loanId);

    	return response()->json(
    		$response
    	, $response['status_code']);
    }
}
