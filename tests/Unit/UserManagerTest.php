<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Manager\UserManager;

class UserManagerTest extends TestCase
{
    public function testNewUserAdd()
    {
    	$userManager = new UserManager();
        $dataToBeInserted = array(
        	'first_name' => 'Sidhant',
        	'last_name' => 'Aggarwal',
        	'email' => 'sidhant92@hotmail.com',
        	'phone' => '+919960567721',
        	'city_id' => '1'
        );

        $userManager->createNewUser($dataToBeInserted);
 
        $this->assertDatabaseHas('users', [
        	'email' => 'sidhant92@hotmail.com'
    	]);
    }

    public function testNewUserAddFail()
    {
    	$userManager = new UserManager();
        $dataToBeInserted = array(
        	'first_name' => 'Sidhant',
        	'last_name' => 'Aggarwal',
        	'email' => 'sidhant92222@hotmail.com',
        	'phone' => '+919960567721',
        	'city_id' => '2345234'
        );

        $userManager->createNewUser($dataToBeInserted);
 
        $this->assertDatabaseMissing('users', [
        	'email' => 'sidhant92222@hotmail.com'
    	]);
    }

    public function testDuplicateEntryFailure()
    {
    	$userManager = new UserManager();
        $dataToBeInserted = array(
        	'first_name' => 'abc',
        	'last_name' => 'Aggarwal',
        	'email' => 'sidhant92222@hotmail.com',
        	'phone' => '+919960567721',
        	'city_id' => '2345234'
        );

        $userManager->createNewUser($dataToBeInserted);
 
        $this->assertDatabaseMissing('users', [
        	'first_name' => 'sidhant92@hotmail.com'
    	]);
    }
}
