<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Manager\LoanManager;

class LoanManagerTest extends TestCase
{
    public function testGetRepaymentsData()
    {
    	$loanManager = new LoanManager();

        $repaymentsData = $loanManager->getRepaymentsData(1220, array('2018-11-05', '2018-12-05'), 45);

        $this->assertTrue(count($repaymentsData) === 2);
    }

    public function testCreateNewYearlyLoan()
    {
        $loanManager = new LoanManager();

        $loanData = array(
            'principal_amount' => 445000,
            'payment_frequency' => 'YEARLY',
            'repayment_count' => 4,
            'interest_rate' => 12.0,
            'processing_fee' => 0,
            'user_id' => 1
        );

        $response = $loanManager->createNewLoan($loanData);

        $this->assertTrue(count($response['repayments_info']) === 4);
        $this->assertTrue($response['repayments_info'][0]['amount_pending'] === 146509.0);
    }

    public function testCreateNewMonthlyLoan()
    {
        $loanManager = new LoanManager();

        $loanData = array(
            'principal_amount' => 445000,
            'payment_frequency' => 'MONTHLY',
            'repayment_count' => 48,
            'interest_rate' => 12.0,
            'processing_fee' => 0,
            'user_id' => 1
        );

        $response = $loanManager->createNewLoan($loanData);

        $this->assertTrue(count($response['repayments_info']) === 48);
        $this->assertTrue($response['repayments_info'][0]['amount_pending'] === 11719.0);  
    }

    public function testCreateNewLoanDatabaseEntry()
    {
        $loanManager = new LoanManager();

        $loanData = array(
            'principal_amount' => 445000,
            'payment_frequency' => 'MONTHLY',
            'repayment_count' => 48,
            'interest_rate' => 12.0,
            'processing_fee' => 0,
            'user_id' => 1
        );

        $response = $loanManager->createNewLoan($loanData);

        $loanId = $response['loan_id'];

        $this->assertDatabaseHas('user_loans', [
            'id' => $loanId
        ]); 
    }

    public function testCreateNewLoanDatabaseRepaymentsEntry()
    {
        $loanManager = new LoanManager();

        $loanData = array(
            'principal_amount' => 445000,
            'payment_frequency' => 'MONTHLY',
            'repayment_count' => 48,
            'interest_rate' => 12.0,
            'processing_fee' => 0,
            'user_id' => 1
        );

        $response = $loanManager->createNewLoan($loanData);

        $loanId = $response['loan_id'];

        $this->assertDatabaseHas('repayments', [
            'loan_id' => $loanId
        ]); 
    }
}
